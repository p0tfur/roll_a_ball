﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text scoreText;
    public Text winText;

    private Rigidbody rb;
    private int scoreCount;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        scoreCount = 0;
        SetCountText();
        winText.text = "";

        if(speed == 0)
        {
            speed = 10;
        }
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            scoreCount += 1;
            SetCountText();
        }
    }

    void SetCountText()
    {
        scoreText.text = "Count: " + scoreCount.ToString();
        if (scoreCount >= 8)
        {
            winText.text = "You won!";
        }
    }
}
